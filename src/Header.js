import React, { Component } from 'react';
import { Card } from 'antd';
import './Header.css';

class Header extends Component {
    render() {
        return (
            <Card
                title="weather app header"
            >
                <p>
                    You are at {this.props.location}
                </p>
            </Card>
        );
    }
}

export default Header;
