import React, { Component } from 'react';
import { Card } from 'antd';
import './Current.css';

class Current extends Component {
    render() {
        return (
            <Card
                title="weather app current"
            >
                <p>
                    {this.props.current}
                </p>
            </Card>
        );
    }
}

export default Current;
