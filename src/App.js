import React, { Component } from 'react';
import { Row, Col } from 'antd';
import './App.css';
import Header from './Header';
import Current from './Current';
import Forcast from './Forcast';
import axios from 'axios'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '...',
      zip: '...',
      current: '...'
    };
    this.getLocationByIp();
  }
  getLocationByIp() {
    axios.get('http://ip-api.com/json')
      .then(response => {
        console.log(response);
        this.setState({
          location: response.data.city,
          zip: response.data.zip,
        });
        this.getCurrentWeatherByZipcode(this.state.zip);
      })
      .catch(error => {
        console.log(error);
      });
  }


  getCurrentWeatherByZipcode(zipcode) {
    axios.get('http://api.openweathermap.org/data/2.5/weather?zip=' + zipcode + ',us&APPID=d44d9be64f09ca77ee59814aea548906')
      .then(response => {
        console.log(response);
        this.setState({
          current: response.data.weather[0].description,
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({
          current: error,
        });
      });
  }
  render() {
    return (
      <div>
        <Row>
          <Col span={12} offset={6}>
            <Header location={this.state.location}/>
            <Current current={this.state.current}/>
            <Forcast />
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
